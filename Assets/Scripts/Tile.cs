﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MCB
{
    public class Tile : MonoBehaviour
    {
        private SpriteRenderer display;
        private int clicks = 0;

        public float width => transform.localScale.x;
        public float height => transform.localScale.y;

        public TileData tileData { get; set; }

        public delegate void Clicked(Tile tile);
        public static event Clicked OnClicked;

        private void Awake()
        {
            display = GetComponentInChildren<SpriteRenderer>();
            tileData = new TileData();
        }

        private void OnMouseDown()
        {
            OnClicked?.Invoke(this);
        }

        public void SetColor(Color color)
        {
            display.color = color;
        }

        public void Clear()
        {
            display.color = Color.white;
        }

        private Color GetRandomColor()
        {
            float red = Random.Range(0, 1f);
            float green = Random.Range(0, 1f);
            float blue = Random.Range(0, 1f);
            return new Color(red, green, blue);
        }

        private Color GetNextColorRGB()
        {
            Color c;
            switch (clicks)
            {
                case 0:
                    c = Color.red;
                    break; 
                case 1:
                    c = Color.green;
                    break;
                case 2:
                    c = Color.blue;
                    break;
                default:
                    c = Color.red;
                    clicks = 0;
                    break;
            }

            return c;
        }

        private void OnEnable()
        {
            TacticToeServer.OnTileUpdated += OnTileUpdated;
        }

        private void OnDisable()
        {
            TacticToeServer.OnTileUpdated -= OnTileUpdated;
        }

        private void OnTileUpdated(Dictionary<string, object> data)
        {
            if (data == null)
            {
                return;
            }

            if (!data.ContainsKey("posX")
                || !data.ContainsKey("posY"))
            {
                return;
            }

            if ((int)data["posX"] != tileData.GridPosition.x
                || (int)data["posY"] != tileData.GridPosition.y)
            {
                return;
            }

            Color c;
            if (!data.ContainsKey("r") 
                || !data.ContainsKey("g")
                || !data.ContainsKey("b")
                || !data.ContainsKey("a"))
            {
                Debug.LogError("Missing values for some reason.");
                return;
            }
            
            c.r = (float)data["r"];
            c.g = (float)data["g"];
            c.b = (float)data["b"];
            c.a = (float)data["a"];
            display.color = c;
        }
    }

}
