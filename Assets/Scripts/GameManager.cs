﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MCB
{
    public class GameManager : MonoBehaviour
    {   
        public static GameManager Instance { get; private set; }
        
        public GameModeType GameMode { get; set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else if (Instance != this)
            {
                Destroy(gameObject);
                return;
            }

            DontDestroyOnLoad(gameObject);
            name = "Game Manager Singleton";
        }
    }
}
