﻿using System.Collections.Generic;
using UnityEngine;

namespace MCB
{
    public class GameScene : StateMachine
    {
        [SerializeField] private Grid grid;
        [SerializeField] private PlayerHUD playerHUD1;
        [SerializeField] private PlayerHUD playerHUD2;
        
        private TacticToeServer server;
        
        private void OnEnable()
        {
            AddListeners();
        }

        private void OnDisable()
        {
            RemoveListeners();
        }

        private void Awake()
        {
            server = TacticToeServer.Instance;
        }

        private void Start()
        {
            // If playing this from the GameScene the mode will not be set so default to PvP.
            if (GameManager.Instance.GameMode == GameModeType.None)
            {
                GameManager.Instance.GameMode = GameModeType.PlayerVsPlayer;
            }
            
            ChangeState<InitGameState>();
        }

        public void StartGame()
        {
            ShowGrid();
            server.ProcessInputEvent(InputEventType.RequestStartGame, new Dictionary<string, object>
            {
                {"gameMode", GameManager.Instance.GameMode.ToString()}
            });
        }
        
        private void AddListeners()
        {
            Tile.OnClicked += OnTileClicked;
            TacticToeServer.OnGameEvent += OnGameEvent;
        }
        
        private void RemoveListeners()
        {
            Tile.OnClicked -= OnTileClicked;
            TacticToeServer.OnGameEvent -= OnGameEvent;
        }
        
        private void OnTileClicked(Tile tile)
        {
            server.ProcessInputEvent(InputEventType.TileClicked, new Dictionary<string, object>
            {
                {"posX", tile.tileData.GridPosition.x},
                {"posY", tile.tileData.GridPosition.y}
            });
        }
        
        private void OnGameEvent(GameEventType eventType, Dictionary<string, object> data)
        {
            switch (eventType)
            {
                case GameEventType.GameCreated:
                    if (!Util.GetValueFromData("tiles", data, out TileData[,] tileData)) return;
                    if (!Util.GetValueFromData("players", data, out PlayerData[] players)) return;
                    if (!Util.GetValueFromData("gameMode", data, out string gameModeString)) return;
                    
                    grid.Generate(tileData);
                    playerHUD1.UpdateUI(players[0]);
                    playerHUD2.UpdateUI(players[1]);
                    break;
                case GameEventType.PlayerTurnChanged:
                    if (!Util.GetValueFromData("lastClickedTile", data, out TileData lastClickedTile)) return;
                    if (!Util.GetValueFromData("players", data, out players)) return;
                    
                    playerHUD1.UpdateUI(players[0]);
                    playerHUD2.UpdateUI(players[1]);

                    Color color = lastClickedTile.ownerId == players[0].id ? Color.blue : Color.red;
                    grid.UpdateTileColor(lastClickedTile, color);
                    break;
                default:
                    Debug.Log("Unhandled Event Type");
                    break;
            } 
        }
        
        public void ShowGrid()
        {
            grid.gameObject.SetActive(true);
        }

        public void HideGrid()
        {
            grid.gameObject.SetActive(false);
        }
    }
}
