﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MCB
{
    public class InitGameState : GameSceneState
    {
        private bool initialized;
        
        public void Start()
        {
            Initialize();
        }
        
        private void Initialize()
        {
            if (initialized)
            {
                return;
            }
            
            owner.StartGame();
            initialized = true;
        }
    }
}
