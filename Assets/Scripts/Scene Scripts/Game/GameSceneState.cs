﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MCB
{
    public class GameSceneState : State
    {
        protected GameScene owner;

        private void Awake()
        {
            owner = GetComponent<GameScene>();
        }
    }
}
