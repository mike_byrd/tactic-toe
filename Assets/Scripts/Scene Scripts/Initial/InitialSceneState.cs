﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MCB
{
    /// <summary>
    /// Handles any kind of initialization that needs to happen.
    /// </summary>
    public class InitialSceneState : State
    {
        protected InitialScene owner;

        private void Awake()
        {
            owner = GetComponent<InitialScene>();
        }

        public override void Enter()
        {
            base.Enter();
            SceneManager.LoadScene("Title");
        }
    }
}
