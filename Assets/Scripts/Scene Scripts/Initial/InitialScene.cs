﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MCB
{
    public class InitialScene : StateMachine
    {
        private void Start()
        {
            ChangeState<InitialSceneState>();
        }
    }
}
