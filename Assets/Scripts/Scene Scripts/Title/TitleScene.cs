﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace MCB
{
    public class TitleScene : StateMachine
    {
        [SerializeField] private Button playerVsPlayerButton;
        [SerializeField] private Button playerVsAiButton;
        [SerializeField] private Button exitGameButton;

        private void OnEnable()
        {
            playerVsPlayerButton.onClick.AddListener(OnVsPlayerButtonClicked);
            playerVsAiButton.onClick.AddListener(OnVsAiButtonClicked);
            exitGameButton.onClick.AddListener(OnExitGameButtonClicked);
        }
    
        private void OnDisable()
        {
            playerVsPlayerButton.onClick.RemoveListener(OnVsPlayerButtonClicked);
            playerVsAiButton.onClick.RemoveListener(OnVsAiButtonClicked);
            exitGameButton.onClick.RemoveListener(OnExitGameButtonClicked);
        }

        private void OnVsPlayerButtonClicked()
        {
            GameManager.Instance.GameMode = GameModeType.PlayerVsPlayer;
            SceneManager.LoadScene("Game");
        }
    
        private void OnVsAiButtonClicked()
        {
            
        }
    
        private void OnExitGameButtonClicked()
        {
#if UNITY_EDITOR
            EditorApplication.ExitPlaymode();     
#else
            Application.Quit();  
#endif
        }
    }
}
