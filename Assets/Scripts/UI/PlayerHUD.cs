﻿using TMPro;
using UnityEngine;

namespace MCB
{
    public class PlayerHUD : MonoBehaviour
    {
        [SerializeField] private TMP_Text playerNameLabel;
        [SerializeField] private TMP_Text statusLabel;
        [SerializeField] private TMP_Text winsValueLabel;

        public void UpdateUI(PlayerData player)
        {
            playerNameLabel.text = player.name;
            winsValueLabel.text = player.wins.ToString();
        
            UpdateStatus(player);

            UpdateNameColor(player.id);
        }

        private void UpdateStatus(PlayerData player)
        {
            if (player.isActivePlayer)
            {
                statusLabel.text = "Your Turn";
                statusLabel.color = Color.green;
            }
            else
            {
                statusLabel.text = "Waiting...";
                statusLabel.color = Color.yellow;
            }
        }

        private void UpdateNameColor(int playerId)
        {
            switch (playerId)
            {
                case 1:
                    playerNameLabel.color = Color.blue;
                    break;
                case 2:
                    playerNameLabel.color = Color.red;
                    break;
                default:
                    playerNameLabel.color = Color.white;
                    break;
            }
        }
    }
}
