﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Util
{
    public static bool ParseObjectValue<T>(object source, out T result) where T : class
    {
        result = source as T;
        if (result == null)
        {
            Debug.LogError($"Failed to parse object value from: {source}. Type expected: {typeof(T)}");
            return false;
        }

        return true;
    }
        
    public static bool ParseBoolValue(object source, out bool result, bool ignoreWarning = false)
    {
        bool? target = source as bool?;
        if (target == null)
        {
            if (!ignoreWarning)
            {
                Debug.LogError($"Failed to parse bool value from: {source}.");
            }
            result = false;
            return false;
        }

        result = target.Value;
        return true;
    }
    
    public static bool ParseIntValue(object source, out int result, bool ignoreWarning = false)
    {
        int? target = source as int?;
        if (target == null)
        {
            if (!ignoreWarning)
            {
                Debug.LogError($"Failed to parse bool value from: {source}.");
            }
            result = -1;
            return false;
        }

        result = target.Value;
        return true;
    }

    public static bool GetValueFromData<T>(string key, Dictionary<string, object> data, out T result) where T : class
    {
        if (data == null)
        {
            result = null;
            Debug.Log("Data was null");
            return false;
        }

        if (!data.ContainsKey(key))
        {
            result = null;
            Debug.Log($"Data did not contain key: {key}");
            return false;
        }

        return ParseObjectValue(data[key], out result);
    }

    public static bool GetIntFromData(string key, Dictionary<string, object> data, out int result)
    {
        if (data == null)
        {
            result = -1;
            Debug.Log("Data was null");
            return false;
        }
        
        if (!data.ContainsKey(key))
        {
            result = -1;
            Debug.Log($"Data did not contain key: {key}");
            return false;
        }

        return ParseIntValue(data[key], out result);
    }
}
