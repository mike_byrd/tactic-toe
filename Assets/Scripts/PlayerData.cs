﻿public enum PlayerType
{
    Human,
    Ai
}

public class PlayerData
{
    public int id;
    public string name;
    public PlayerType type;
    public bool isActivePlayer;
    public int wins;
}
