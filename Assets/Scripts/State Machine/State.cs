﻿using System;
using UnityEngine;

namespace MCB
{
    public abstract class State : MonoBehaviour
    {
        protected bool isReady = false;
        public virtual void Enter()
        {
            AddListeners();
            isReady = false;
        }

        public virtual void Exit()
        {
            RemoveListeners();
            isReady = false;
        }

        protected virtual void OnDestroy()
        {
            RemoveListeners();
        }

        protected virtual void AddListeners()
        {
        }

        protected virtual void RemoveListeners()
        {
        }

        protected bool DelayUpdate()
        {
            if (!isReady)
            {
                isReady = true;
                return isReady;
            }

            return false;
        }
    }
}