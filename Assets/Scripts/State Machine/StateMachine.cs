﻿using UnityEngine;
using System.Collections;

namespace MCB
{
    public class StateMachine : MonoBehaviour
    {
        public virtual State CurrentState
        {
            get => _currentState;
            set => Transition(value);
        }
        
        public virtual State PreviousState
        {
            get => _previousState;
            private set => _previousState = value;
        }

        protected State _previousState;
        protected State _currentState;
        protected bool _inTransition;

        public virtual T GetState<T>() where T : State
        {
            T target = GetComponent<T>();
            if (target == null)
                target = gameObject.AddComponent<T>();
            return target;
        }

        public virtual void ChangeState<T>() where T : State
        {
            CurrentState = GetState<T>();
        }
        
        public virtual void ChangeToPreviousState()
        {
            if (_previousState == null) return;
            Transition(_previousState);
        }

        protected virtual void Transition(State value)
        {
            if (_currentState == value || _inTransition)
                return;

            _inTransition = true;

            if (_currentState != null)
                _currentState.Exit();

            _previousState = _currentState;
            _currentState = value;

            if (_currentState != null)
                _currentState.Enter();

            _inTransition = false;
        }
    }
}