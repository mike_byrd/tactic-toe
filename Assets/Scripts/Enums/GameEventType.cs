﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameEventType
{
    GameCreated,
    PlayerTurnChanged,
    TileClicked
}
