﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MCB
{
    public enum GameModeType
    {
        None,
        PlayerVsPlayer,
        PlayerVsAi
    }
}
