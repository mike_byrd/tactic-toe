﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MCB
{
    public class Grid : MonoBehaviour
    {
        [SerializeField] private Tile tilePrefab;
        [SerializeField] private Transform tileContainer;

        private Tile[,] tiles;

        public void Generate(TileData[,] tileData)
        {   
            Clear();

            int lengthX = tileData.GetLength(0);
            int lengthY = tileData.GetLength(1);
            tiles = new Tile[lengthX, lengthY];
            
            for (int y = 0; y < lengthY; y++)
            {
                for (int x = 0; x < lengthX; x++)
                {
                    Tile t = Instantiate(tilePrefab, tileContainer);
                    t.name = $"Tile ({x}, {y})";
                    t.tileData = tileData[x, y];

                    Vector2 position = new Vector2 {
                        x = t.tileData.GridPosition.x * t.width - t.width, 
                        y = t.tileData.GridPosition.y * t.height - t.height
                    };
                    
                    t.transform.position = position;
                    tiles[x, y] = t;
                }
            }
        }

        public void UpdateTileColor(TileData tileData, Color color)
        {
            Tile t = tiles[tileData.GridPosition.x, tileData.GridPosition.y];
            t.SetColor(color);
        }

        public void Clear()
        {
            DestroyExistingChilldren();
            tiles = null;
        }
        
        private void DestroyExistingChilldren()
        {
            foreach (Transform t in tileContainer)
            {
                Destroy(t.gameObject);
            }
        }
    }
}
