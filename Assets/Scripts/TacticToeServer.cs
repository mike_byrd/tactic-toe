﻿using System;
using System.Collections.Generic;

namespace MCB
{
    public class TacticToeServer
    {
        private static TacticToeServer instance;
        public static TacticToeServer Instance => instance ?? (instance = new TacticToeServer());

        public const int MaxPlayers = 2;

        private bool initialized;
        private TileData[,] tiles;

        public delegate void TileUpdated(Dictionary<string, object> data);
        public static event TileUpdated OnTileUpdated;

        public delegate void GameEvent(GameEventType type, Dictionary<string, object> data);
        public static event GameEvent OnGameEvent;

        private readonly Random rand;
        
        private PlayerData[] players;
        private PlayerData activePlayer;
        private int currentTurn;
        private GameModeType gameMode;

        public TacticToeServer()
        {   
            rand = new Random(Guid.NewGuid().GetHashCode());
        }

        private void CreateGame(GameModeType mode)
        {
            switch (mode)
            {
                case GameModeType.PlayerVsPlayer:
                    CreatePlayerVsPlayerGame();
                    break;
                case GameModeType.PlayerVsAi:
                    CreatePlayerVsAiGame();
                    break;
            }
        }

        private void CreateGrid()
        {
            tiles = new TileData[TileData.MaxTilesX, TileData.MaxTilesY];
            
            TileData tile;
            for (int y = 0; y < TileData.MaxTilesY; y++)
            {
                for (int x = 0; x < TileData.MaxTilesX; x++)
                {
                    tile = new TileData
                    {
                        GridPosition = new Point(x, y)
                    };

                    tiles[x, y] = tile;
                }
            }
        }

        private void CreatePlayerVsPlayerGame()
        {
            CreateGrid();
            
            players = new []
            {
                new PlayerData { id = 1, name = "Player 1", type = PlayerType.Human}, 
                new PlayerData { id = 2, name = "Player 2", type = PlayerType.Human}, 
            };

            activePlayer = players[0];
            activePlayer.isActivePlayer = true;
            gameMode = GameModeType.PlayerVsPlayer;
            
            Dictionary<string, object> data = new Dictionary<string, object>
            {
                {"tiles", tiles},
                {"players", players},
                {"gameMode", gameMode.ToString()}
            };
            
            OnGameEvent(GameEventType.GameCreated, data);
        }

        

        private void CreatePlayerVsAiGame()
        {
            // For now just create a PvP game.
            CreatePlayerVsPlayerGame();
        }

        public void ProcessInputEvent(InputEventType eventType, Dictionary<string, object> data)
        {
            if (data == null)
            {
                return;
            }

            Dictionary<string, object> returnData;
            switch (eventType)
            {
                case InputEventType.TileClicked:
                    if (!Util.GetIntFromData("posX", data, out int x)) return;
                    if (!Util.GetIntFromData("posY", data, out int y)) return;

                    TileData tile = tiles[x, y];
                    if (tile.HasOwner)
                    {
                        return;
                    }
                    
                    tile.ownerId = activePlayer.id;
                    NextTurn();

                    returnData = new Dictionary<string, object>
                    {
                        {"lastClickedTile", tile},
                        {"players", players}
                    };
                    
                    OnGameEvent(GameEventType.PlayerTurnChanged, returnData);
                    break;
                case InputEventType.RequestStartGame:
                    if (!Util.GetValueFromData("gameMode", data, out string gameModeString)) return;
                    if (!Enum.TryParse(gameModeString, out GameModeType mode)) return;
                    
                    CreateGame(mode);
                    break;
            }
        }

        private void NextTurn()
        {
            activePlayer.isActivePlayer = false;
            
            currentTurn++;
            int nextPlayerIndex = currentTurn % players.Length;
            
            players[nextPlayerIndex].isActivePlayer = true;
            activePlayer = players[nextPlayerIndex];
        }
    }
}
