﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct ColorData
{
    public float red;
    public float green;
    public float blue;
    public float alpha;
}

public struct Point
{
    public int x;
    public int y;

    public Point(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
}

public class TileData
{
    public const int MaxTilesX = 3;
    public const int MaxTilesY = 3;
    
    public ColorData colorData;
    public int ownerId;

    public ColorData ColorData => colorData;

    private Point gridPosition;
    public Point GridPosition
    {
        get => gridPosition;
        set
        {
            if (value.x < 0 || value.y < 0
                || value.x > MaxTilesX || value.y > MaxTilesY)
            {
                Debug.LogWarning($"TileData.GridPosition (Set): {value} is outside the bounds of the grid. " +
                                 $"This value will not be set.");
                return;
            }

            gridPosition = value;
        }
    }

    public void UpdateColorData(float red, float green, float blue, float alpha = 1.0f)
    {
        colorData.red = red;
        colorData.green = green;
        colorData.blue = blue;
        colorData.alpha = alpha;
    }

    public bool HasColor => colorData.red > 0 || colorData.green > 0 || colorData.blue > 0;
    public bool HasOwner => ownerId > 0;
}
